//
//  BaseMacro.swift
//  GKGame_Swift
//
//  Created by wangws1990 on 2018/9/30.
//  Copyright © 2018 wangws1990. All rights reserved.
//
import UIKit

@_exported import Hue
@_exported import ATKit_Swift
@_exported import ATRefresh_Swift
@_exported import SnapKit
@_exported import RxSwift
@_exported import RxCocoa
@_exported import MGJRouter_Swift

let AppWindow = BaseMacro.window()

let SCREEN_WIDTH  :CGFloat  = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT :CGFloat  = UIScreen.main.bounds.size.height

let iPhone_X        : Bool      = at_iphoneX
let STATUS_BAR_HIGHT:CGFloat    = at_statusBar //状态栏
let NAVI_BAR_HIGHT  :CGFloat    = at_naviBar  //导航栏
let TAB_BAR_ADDING  :CGFloat    = at_tabBar  //iphoneX斜刘海

let AppRadius    :CGFloat = 3

let appDatas : [String] = ["七界传说","极品家丁","择天记","神墓","遮天","不死不灭","吞噬星空","盘龙"]
//更换皮肤的通知
public let AppThemeNotification = NSNotification.Name(rawValue: "AppThemeNotification")
public let AppVolumeNotication  = NSNotification.Name(rawValue: "SystemVolumeDidChange")

class BaseMacro{
    public static func appFrame() ->CGRect{
        let AppTop : CGFloat = 0
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        if width < height {
            let top = STATUS_BAR_HIGHT + 15
            let bottom = TAB_BAR_ADDING + 20
            return CGRect(x:20, y:top, width: width - 40, height:CGFloat(height - top - bottom))
        }else{
            var insets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            if #available(iOS 11.0, *) {
                if let window = UIApplication.shared.windows.first {
                    insets = window.safeAreaInsets
                }
            }
            let letft = AppTop + insets.left
            let right = AppTop + insets.right
            let bottom = insets.bottom > 0 ? (insets.bottom + 10) : 20
            let top : CGFloat = 5
            return CGRect(x:letft, y:top, width: width - letft - right, height: height - top - bottom)
        }
    }
    public static func iPhone_Bar() ->(iphoneX :Bool,statusBar : CGFloat,tabBar : CGFloat){
        if #available(iOS 11.0, *) {
            guard let  window = UIApplication.shared.delegate?.window,let inset = window?.safeAreaInsets else { return (false,20,0) }
            return (inset.bottom > 0, inset.top > 0 ? inset.top : 20,inset.bottom)
        } else {
            return (false,20,0)
        }
    }
    fileprivate static func window() -> UIWindow{
        let application  = UIApplication.shared
        guard let window = application.delegate?.window else {
            if let window = application.windows.first {
                return window
            }
            return UIWindow(frame: UIScreen.main.bounds)
        }
        return window!
    }
}
extension UIImage{
    public static func imageWithColor(color:UIColor) -> UIImage{
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        guard let context = UIGraphicsGetCurrentContext() else { return UIImage() }
        context.setFillColor(color.cgColor)
        context.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? UIImage()
     }
}
