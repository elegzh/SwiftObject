//
//  GKSearchData.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/20.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKSearchData{
    public static func insertKeyWord(keyWord : String,completion :@escaping ((_ success : Bool) ->())){
        BaseSearchQueue().insertData(keyword: keyWord, completion: completion)
    }
    public static func deleteKeyWord(keyWord : String,completion :@escaping ((_ success : Bool) ->())){
        BaseSearchQueue().deleteData(keyword: keyWord, completion: completion)
    }
    public static func deleteKeyWord(datas : [String],completion :@escaping ((_ success : Bool) ->())){
        BaseSearchQueue().deleteData(keywords: datas, completion: completion)
    }
    public static func getKeyWords(page:Int,size:Int,completion :@escaping ((_ listDatas : [String]) ->())){
        BaseSearchQueue().searchData(page: page, size: size, completion: completion)
    }
}
