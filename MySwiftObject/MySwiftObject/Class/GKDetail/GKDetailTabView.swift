//
//  GKDetailTabView.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
//
class GKDetailTabView: BaseView {
    @IBOutlet weak var readBtn: UIButton!
    @IBOutlet weak var leftView: UIView!
    lazy var favBtn: GKFavButton = {
        return GKFavButton(frame: CGRect(x:SCREEN_WIDTH/6-22, y:2, width: 45, height: 45), image: UIImage(named: "icon_star"))
    }()
    override func awakeFromNib() {
        self.leftView.addSubview(self.favBtn)
        self.favBtn.imageColorOn = moya.appColor
        self.favBtn.circleColor = moya.appColor
        self.favBtn.lineColor = moya.appColor
        self.leftView.backgroundColor = UIColor.appxffffff()
        self.readBtn.setBackgroundImage(UIImage.imageWithColor(color: moya.appColor), for: .normal)
        self.readBtn.setBackgroundImage(UIImage.imageWithColor(color: moya.appColor), for: .highlighted)
    }
}
