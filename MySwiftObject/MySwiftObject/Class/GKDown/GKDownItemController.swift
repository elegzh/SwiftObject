//
//  GKDownItemController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/4/15.
//  Copyright © 2022 wangws1990. All rights reserved.
//
import UIKit
class GKDownItemController: BaseTableViewController {
    convenience init(model :GKContent) {
        self.init()
        self.model = model
    }
    var model :GKContent? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        if let item = self.model{
            self.showNavTitle(title:item.title)
        }
        self.tableView.snp.remakeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.top.bottom.equalToSuperview()
        }
        self.setupRefresh(scrollView: self.tableView, options: .auto)
    }
    override func refreshData(page: Int) {
        self.model?.pageContent()
        self.tableView.reloadData()
        self.endRefresh(more: false)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let item = self.model else { return 0 }
        return item.pageCount
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let item = self.model else { return 0 }
        return item.size.height
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = self.model else { return UITableViewCell() }
        let cell = BBookScrollTableCell.cellForTableView(tableView: tableView, indexPath: indexPath)
        cell.setModel(model: item, indexPath: indexPath)
        return cell
    }
}

