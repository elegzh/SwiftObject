//
//  GKItemViewController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/10.
//  Copyright © 2018 wangws1990. All rights reserved.
//
//  
import UIKit
protocol GKBookDelegate :NSObjectProtocol{
    func viewDidAppear(chapter :NSInteger,pageIndex :NSInteger,content :GKContent?)
}
class GKNovelController: BaseViewController {
    public weak var delegate : GKBookDelegate? = nil
    public var chapter  :NSInteger = 0
    public var pageIndex:NSInteger = 0
    public var content : NSAttributedString?{
        return self.readView.content
    }
    public var emptyData:Bool?{
        didSet{
            guard let empty = emptyData else { return }
            self.emptyDataImageV.isHidden = empty ? true : false
        }
    }
    public func setModel(model :GKContent,chapter:NSInteger,pageIndex :NSInteger){
        self.batteryView.isHidden = model.empty
        self.model = model
        self.pageIndex = pageIndex
        self.chapter = chapter
        self.titleLab.text = model.title
        if model.empty {
            self.emptyDataImageV.isHidden = false
        }else{
            self.emptyDataImageV.isHidden = true
            self.readView.content = model.attContent(pageIndex)
            self.batteryView.loadData(pageIndex: pageIndex + 1, total: model.pageCount)
        }
    }
    private var model :GKContent? = nil
    private lazy var readView: GKNovelView = {
        var readView = GKNovelView(frame: BaseMacro.appFrame())
        readView.backgroundColor = UIColor.clear;
        return readView
    }()
    private lazy var mainView: UIImageView = {
        let mainView : UIImageView = UIImageView()
        mainView.isUserInteractionEnabled = false
        return mainView;
    }()
    private lazy var emptyDataImageV: UIImageView = {
        let mainView : UIImageView = UIImageView()
        mainView.isUserInteractionEnabled = false
        mainView.image = UIImage(named: "icon_data_empty")
        mainView.isHidden = true
        return mainView
    }()
    private lazy var titleLab: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .left
        return label
    }()
    private lazy var batteryView: GKBookBatteryView = {
        var readView = GKBookBatteryView()
        readView.backgroundColor = UIColor.clear;
        readView.isHidden = true
        return readView
    }()
    deinit {
        self.removeNotification()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.mainView)
        self.mainView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        self.view.addSubview(self.readView)
        self.view.addSubview(self.titleLab)
        self.view.addSubview(self.batteryView)
        self.view.addSubview(self.emptyDataImageV)
        self.emptyDataImageV.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        moya.bookSet.landscape ? self.landscape() : self.portrait()
        setTheme()
        self.addNotification()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let delegate = self.delegate else { return }
        delegate.viewDidAppear(chapter: self.chapter, pageIndex: self.pageIndex,content:self.model)
    }
    func addNotification(){
        NotificationCenter.default.addObserver(forName: UIDevice.batteryLevelDidChangeNotification, object: nil, queue:OperationQueue.main) { [weak self] nation in
            debugPrint(nation)
            self?.batteryView.reloadThemeUI()
        }
    }
    func removeNotification(){
        NotificationCenter.default.removeObserver(self, name: UIDevice.batteryLevelDidChangeNotification, object: nil)
    }
    private func portrait(){
        self.titleLab.isHidden = false
        self.readView.frame = BaseMacro.appFrame()
        self.titleLab.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(STATUS_BAR_HIGHT - 10)
        }
        self.batteryView.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(20 + TAB_BAR_ADDING)
        }
    }
    private func landscape(){
        self.titleLab.isHidden = true
        self.readView.frame = BaseMacro.appFrame()
        self.batteryView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(0)
            make.height.equalTo(20)
        }
    }
    private func setTheme(){
        let hex = moya.bookSet.theme.themeColor
        self.mainView.image = UIImage.imageWithColor(color: UIColor(hex: hex))
        self.titleLab.textColor = UIColor(hex: moya.bookSet.theme.fontColor)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews();
        self.view.frame.size.width > self.view.frame.height ? self.landscape() : self.portrait()
    }
    override var shouldAutorotate: Bool{
        return true
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return [.portrait,.landscapeRight]
    }
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        return .portrait
    }
}

