//
//  GKBookSetBrowseCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/12.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
class GKBookSetBrowseCell: BaseCollectionCell {

    @IBOutlet weak var titleLab: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLab.tag = 10086
        self.titleLab.isUserInteractionEnabled = true
        self.titleLab.layer.masksToBounds = true
        self.titleLab.layer.cornerRadius = 5
    }
    var model : GKNovelBrowse?{
        didSet{
            guard let item = model else { return }
            let select = item == moya.bookSet.browse
            self.titleLab.text = item.rawValue
            self.titleLab.textColor = select ? UIColor.white : UIColor.appx333333()
            self.titleLab.backgroundColor = select ? UIColor.appColor() : UIColor.appxf8f8f8()
        }
    }

}
