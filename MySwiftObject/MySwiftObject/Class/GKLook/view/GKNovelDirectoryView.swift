//
//  GKNovelDirectoryView.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/19.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import SwiftyJSON
@objc protocol GKNovelDirectoryDelegate : NSObjectProtocol {
    @objc optional func selectChapter(view :GKNovelDirectoryView, chapter:NSInteger)
}
class GKNovelDirectoryView: BaseView{
    var chapter :JSON? = nil
    weak var delegate : GKNovelDirectoryDelegate? = nil
    lazy var listData : [JSON] = {
        return []
    }()
    lazy var tableView : UITableView = {
        let tableView  = UITableView(frame: CGRect.zero, style: UITableView.Style.plain)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60
        tableView.keyboardDismissMode = .onDrag
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: NSStringFromClass(UICollectionViewCell.classForCoder()))
        tableView.backgroundColor = UIColor.appxf8f8f8()
        tableView.backgroundView?.backgroundColor = UIColor.appxf8f8f8()
        return tableView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.tableView.contentInset = UIEdgeInsets(top:STATUS_BAR_HIGHT, left: 0, bottom: 0, right: 0)
        self.loadUI();
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:  aDecoder);
        self.loadUI();
    }
    func setDatas(listData:[JSON]){
        self.listData.removeAll();
        self.listData.append(contentsOf: listData);
        self.tableView.reloadData();
    }
    func loadUI(){
        self.isUserInteractionEnabled = true;
        self.addSubview(self.tableView);
        self.tableView.snp.makeConstraints { (make) in
            make.left.top.bottom.equalToSuperview();
            make.width.equalTo(SCREEN_WIDTH/5*4.0);
        }
    }
}
extension GKNovelDirectoryView :UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  : GKNovelDirectoryCell = GKNovelDirectoryCell.cellForTableView(tableView: tableView, indexPath: indexPath)
        cell.selectionStyle = .none;
        let model = self.listData[indexPath.row];
        if let chapter = self.chapter {
            cell.select = model["id"].rawString()  == chapter["id"].rawString();
        }
        cell.json = model
        return cell;
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
        guard let delegate = self.delegate else { return }
        delegate.selectChapter?(view: self, chapter: indexPath.row)
    }
}
