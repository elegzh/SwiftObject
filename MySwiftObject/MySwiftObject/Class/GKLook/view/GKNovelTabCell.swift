//
//  GKNovelTabCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/13.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKNovelTabCell: BaseCollectionCell {

    @IBOutlet weak var width: NSLayoutConstraint!
    @IBOutlet weak var imageV: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tag = 10086
        self.contentView.tag = self.tag
    }

}
