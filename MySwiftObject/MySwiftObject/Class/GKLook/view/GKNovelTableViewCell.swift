//
//  GKNovelTableViewCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/9/8.
//  Copyright © 2020 wangws1990. All rights reserved.
//  Copyright

import UIKit
private let tableCell = GKNovelTableViewCell.instanceView()
class GKNovelTableViewCell: BaseTableCell {

    @IBOutlet weak var bottom: NSLayoutConstraint!
    @IBOutlet weak var top: NSLayoutConstraint!
    @IBOutlet weak var contentLab: UILabel!
    var model : GKContent?{
        didSet{
            guard let model = model else { return  }
            //   var content  :String = ""
            let attr  = NSMutableAttributedString(string: model.lineContent
                                                  , attributes: (GKBookSet.defaultFont() ))
            self.contentLab.attributedText = attr
        }
    }
    var pageIndex :NSInteger = 0
    var content :NSAttributedString?{
        didSet{
            guard let item = content else { return }
           // let model :GKBookSet = GKBookSet.manager.config!
            self.contentLab.attributedText = item
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        // Initialization code
    }

    static func heightForModel(width : CGFloat,model :GKContent) ->CGFloat{
        tableCell.model = model
        let widthFenceConstraint = NSLayoutConstraint(item: tableCell.contentView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: width)
        tableCell.contentView.addConstraint(widthFenceConstraint)
        let height = tableCell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        tableCell.contentView.removeConstraint(widthFenceConstraint)
        return height;
    }
    
}
class BBookScrollTableCell :UITableViewCell{
    public var indexPath :IndexPath? = nil
    public var model :GKContent? = nil
    public func setContent(content :NSAttributedString,pageArray :[Int],indexPath :IndexPath,model :GKContent){
        self.indexPath = indexPath
        self.model = model
        self.content = content
    }
    public func setModel(model :GKContent,indexPath :IndexPath){
        self.model = model
        self.indexPath = indexPath
        self.content = model.attContent(indexPath.row)
    }
    private var content :NSAttributedString?{
        didSet{
            guard let model = content else { return }
            self.readView.content = model
        }
    }
    private lazy var readView: GKNovelView = {
        let size = BaseMacro.appFrame().size
        var readView = GKNovelView(frame:CGRect(x:0, y: 0, width: size.width, height:size.height))
        readView.backgroundColor = UIColor.clear
        return readView
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        loadUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadUI()
    }
    func loadUI(){
        self.tag = 1000
        self.contentView.tag = 1000
        self.selectionStyle = .none
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        self.contentView.addSubview(self.readView)
    }
}
