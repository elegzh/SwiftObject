//
//  GKManTabController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/18.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKManTabController: BaseViewController {
    private lazy var listData: [GKManChapter] = {
        return []
    }()
    private var content : GKManContent? = nil
    private var pageCtrl   :UIPageViewController?   = nil
    private var joinedCtrl :UIPageViewController?   = nil
    private var scroll     :GKManTableController?   = nil
    
    lazy var topView: GKNovelTopView = {
        let top = GKNovelTopView.instanceView()
        top.delegate = self
        top.isHidden = true
        return top
    }()
    lazy var tabView: GKManTabView = {
        let top = GKManTabView.instanceView()
        top.delegate = self
        top.isHidden = true
        return top
    }()
    lazy var tap: UITapGestureRecognizer = {
        return UITapGestureRecognizer(target: self, action: #selector(tapAction(sender:)))
    }()
    private var chapter    :Int = 0
    private var pageIndex  :Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fd_prefersNavigationBarHidden = true
        self.fd_interactivePopDisabled = true
        self.loadUI()
        self.loadData()
    }
    @objc func tapAction(sender :UITapGestureRecognizer){
        let p = sender.location(in: self.view)
        if p.y < moya.safe_top + 44 || p.y > moya.safe_height - moya.safe_bottom - 49{
            return
        }
        if self.topView.isHidden{
            self.showTopView()
        }else{
            self.hiddenTopView()
        }
    }
    func showTopView(){
        self.topView.isHidden = false
        self.tabView.isHidden = false
        self.topView.snp.remakeConstraints { make in
            make.left.right.equalToSuperview()
            make.height.equalTo(moya.safe_top + 44)
            make.top.equalToSuperview().offset(0)
        }
        self.tabView.snp.remakeConstraints { make in
            make.left.right.equalToSuperview()
            make.height.equalTo(moya.safe_bottom + 49)
            make.bottom.equalToSuperview().offset(0)
        }
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        } completion: { finish in
            
        }
    }
    func hiddenTopView(){
        self.topView.snp.remakeConstraints { make in
            make.left.right.equalToSuperview()
            make.height.equalTo(moya.safe_top + 44)
            make.top.equalToSuperview().offset(-moya.safe_top - 44)
        }
        self.tabView.snp.remakeConstraints { make in
            make.left.right.equalToSuperview()
            make.height.equalTo(moya.safe_bottom + 49)
            make.bottom.equalToSuperview().offset(moya.safe_bottom + 49)
        }
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        } completion: { finish in
            self.topView.isHidden = true
            self.tabView.isHidden = true
        }
    }
    func loadUI(){
        self.topView.isUserInteractionEnabled = true
        self.tabView.isUserInteractionEnabled = true
        self.topView.isHidden = true
        self.tabView.isHidden = true
        self.view.addSubview(self.topView)
        self.view.addSubview(self.tabView)
        self.topView.snp.remakeConstraints { make in
            make.left.right.equalToSuperview()
            make.height.equalTo(moya.safe_top + 44)
            make.top.equalToSuperview().offset(-moya.safe_top - 44)
        }
        self.tabView.snp.remakeConstraints { make in
            make.left.right.equalToSuperview()
            make.height.equalTo(moya.safe_bottom + 49)
            make.bottom.equalToSuperview().offset(moya.safe_bottom + 49)
        }
        self.view.addGestureRecognizer(self.tap)
        self.loadScrollCtrlUI()
    }
    func loadPageCtrlUI(){
        self.removePageCtrlUI()
        self.removeJoinedCtrl()
        let vc = GKNovelController()
        self.pageCtrl = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .vertical, options: nil)
        guard let pageCtrl = self.pageCtrl else { return  }
        pageCtrl.dataSource = self
        pageCtrl.delegate = self
        pageCtrl.setViewControllers([vc], direction: .forward, animated: false, completion: nil)
        self.addChild(pageCtrl)
        self.view.addSubview(pageCtrl.view)
        self.view.sendSubviewToBack(pageCtrl.view)
        pageCtrl.didMove(toParent: self);
        pageCtrl.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview();
        }
    }
    private func removePageCtrlUI(){
        guard let joinedCtrl = self.pageCtrl else { return  }
        joinedCtrl.view.removeFromSuperview()
        joinedCtrl.removeFromParent()
    }
    func loadJoinedCtrlUI(){
        self.removePageCtrlUI()
        self.removeJoinedCtrl()
        
        let dic = [UIPageViewController.OptionsKey.interPageSpacing : 0]
        let vc = GKNovelController()
        self.joinedCtrl = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: dic)
        guard let joinedCtrl = self.joinedCtrl else { return  }
        joinedCtrl.dataSource = self
        joinedCtrl.delegate = self
        joinedCtrl.setViewControllers([vc], direction: .forward, animated: false, completion: nil)
        self.addChild(joinedCtrl)
        self.view.addSubview(joinedCtrl.view)
        self.view.sendSubviewToBack(joinedCtrl.view)
        joinedCtrl.didMove(toParent: self);
        joinedCtrl.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview();
        }
    }
    private func removeJoinedCtrl(){
        guard let joinedCtrl = self.joinedCtrl else { return  }
        joinedCtrl.view.removeFromSuperview()
        joinedCtrl.removeFromParent()
    }
    func loadScrollCtrlUI(){
        self.removePageCtrlUI()
        self.removeJoinedCtrl()
        self.removeScrollCtrl()
        self.scroll = GKManTableController()
        guard let scroll = self.scroll else { return }
        self.addChild(scroll)
        self.view.addSubview(scroll.view)
        self.view.sendSubviewToBack(scroll.view)
        scroll.didMove(toParent: self);
        scroll.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview();
        }
    }
    private func removeScrollCtrl(){
        guard let joinedCtrl = self.scroll else { return  }
        joinedCtrl.view.removeFromSuperview()
        joinedCtrl.removeFromParent()
    }
    private var beforeCtrl :GKNovelController?{
       // guard let vc = self.showCtrl else { return nil }
        return self.readCtrl
    }
    private var afterCtrl : GKNovelController?{
       // guard let vc = self.showCtrl else { return nil }
    return self.readCtrl
    }
    private var readCtrl :GKNovelController{
        let vc = GKNovelController()
        if self.listData.count > self.chapter {

        }
        return vc
    }
    private var showCtrl :GKNovelController?{
        return nil
    }
    func loadData(){
        GKManCache.getChapters(commicId: "146607") { object in
            if let model = object,let listChapter = model.chapters{
                self.scroll?.setCommic(comicId: "146607", chapter: 0, listChapter: listChapter)
            }
        } failure: { error in
            
        }
    }
    func loadChapterData(_ chapterId :String){
        ApiMoya.request(target: .manchapter(chapterId)) { json in
            guard let content = GKManContent.deserialize(from: json.rawString()) else { return }
            self.content = content
        } failure: { error in
            
        }
    }
}
extension GKManTabController : UIPageViewControllerDelegate,UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return self.beforeCtrl
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return self.afterCtrl
    }
    func pageViewController(_ pageViewController: UIPageViewController, spineLocationFor orientation: UIInterfaceOrientation) -> UIPageViewController.SpineLocation {
//        guard let item = pageViewController.viewControllers?.first else { return .none }
//        if self.browse == .pageCurl{
//            guard let pageCtrl = self.pageCtrl else { return .none}
//            pageCtrl.setViewControllers([item], direction: .forward, animated: true, completion: nil);
//        }else if self.browse == .joined{
//            guard let joinedCtrl = self.joinedCtrl else { return .none}
//            joinedCtrl.setViewControllers([item], direction: .forward, animated: true, completion: nil);
//        }
        return .min;
    }
}
extension GKManTabController :GKManTabDelegate{
    func manTabView(tabView: GKManTabView, type: GKManTabType) {
        
    }
}
extension GKManTabController :GKTopDelegate{
    func topView(topView: GKNovelTopView, back: Bool) {
        self.goBack()
    }
}
