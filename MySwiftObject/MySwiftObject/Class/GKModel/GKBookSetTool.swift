//
//  GKBookSetTool.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/12.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

extension GKBookSet {
    public static var bookSet: GKBookSet = {
        let defaults  = UserDefaults.standard
        let data = defaults.object(forKey: moya.defaultBookSet)
        let json = JSON(data as Any)
        guard let model = GKBookSet.deserialize(from: json.rawString()) else { return GKBookSet() }
        return model
    }()
    static func setNight(_ night:Bool){
        night ? setSkin(skin: .nightTheme()) : setSkin(skin: .defaultlTheme())
        if moya.bookSet.night != night{
            moya.bookSet.night = night
            saveConfig(moya.bookSet)
        }
    }
    static func setLandscape(_ landscape:Bool){
        if moya.bookSet.landscape != landscape{
            moya.bookSet.landscape = landscape
            saveConfig(moya.bookSet)
        }
    }
    static func setTraditiona(_ traditiona:Bool){
        if moya.bookSet.traditiona != traditiona{
            moya.bookSet.traditiona = traditiona
            saveConfig(moya.bookSet)
        }
    }
    static func setBrightness(_ brightness:Float){
        if moya.bookSet.brightness != brightness{
            moya.bookSet.brightness = brightness
            saveConfig(moya.bookSet)
        }
    }
    static func setFontName(_ fontName :String){
        if moya.bookSet.fontName != fontName{
            moya.bookSet.fontName = fontName
            saveConfig(moya.bookSet)
        }
    }
    static func setFont(_ fontSize :Float){
        if moya.bookSet.fontSize != fontSize{
            moya.bookSet.fontSize = fontSize
            saveConfig(moya.bookSet)
        }
    }
    static func setSkin(skin :GKBookTheme){
        if moya.bookSet.theme.themeColor != skin.themeColor{
            moya.bookSet.theme = skin
            saveConfig(moya.bookSet)
        }
    }
    static func setBrowse(browse:GKNovelBrowse){
        if moya.bookSet.browse != browse{
            moya.bookSet.browse = browse
            saveConfig(moya.bookSet)
        }
    }
    static func saveConfig(_ config :GKBookSet){
        let defaults :UserDefaults = UserDefaults.standard
        guard let data = config.toJSON() else { return }
        defaults.set(data, forKey:moya.defaultBookSet)
        defaults.synchronize()
    }
    static func defaultFont()-> [NSAttributedString.Key: Any]{
        let model = moya.bookSet
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = CGFloat(model.lineSpacing )//段落 行间距
        paragraphStyle.firstLineHeadIndent  = 0//CGFloat(model.firstLineHeadIndent )//首行缩进
        paragraphStyle.paragraphSpacingBefore = CGFloat(model.paragraphSpacingBefore ) //段间距，当前段落和上个段落之间的距离。
        paragraphStyle.paragraphSpacing = CGFloat(model.paragraphSpacing ) //段间距，当前段落和下个段落之间的距离。
        paragraphStyle.alignment = .justified//两边对齐
        paragraphStyle.allowsDefaultTighteningForTruncation = true
        var att :[NSAttributedString.Key : Any] = [:]
        let font = UIFont.systemFont(ofSize: CGFloat(model.fontSize))
        let color  = UIColor(hex: model.theme.fontColor)
        att.updateValue(font, forKey: .font)
        att.updateValue(color, forKey: .foregroundColor)
        att.updateValue(paragraphStyle, forKey: .paragraphStyle)
        return att
    }
    static func defaultSkin() ->UIImage{
        let model = GKBookTheme.defaultlTheme()
        guard let url = model.placeholder else { return UIImage.imageWithColor(color: UIColor(hex: model.themeColor)) }
        return UIImage(named: url) ?? UIImage()
    }
}
