//
//  GKBookVolume.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/12.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

struct GKBookVolume: HandyJSON {
    var volume   :Float  = 0
    var reason   :String = ""
    var category :String = ""
    mutating func mapping(mapper: HelpingMapper) {
        mapper <<< self.volume <-- ["Volume","volume","AudioVolume"]
        mapper <<< self.reason <-- ["Reason","reason","AudioVolumeChangeReason"]
        mapper <<< self.category <-- ["AudioCategory","category"]
    }
    var userChage :Bool{
        return self.reason == "ExplicitVolumeChange"
    }
}

