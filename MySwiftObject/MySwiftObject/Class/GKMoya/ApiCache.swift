//
//  GKCache.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/6/23.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit
import YYCache

private let bookCache = YYCache(name:"bookCache")
class ApiCache {
    public class func removeAll(){
        guard let cache = bookCache else { return  }
        cache.removeAllObjects()
    }
    public class func clear(key :String){
        guard let cache = bookCache else { return  }
        cache.removeObject(forKey: key)
    }
    public class func set(object : [String:Any],key :String){
        guard let cache = bookCache else { return  }
        cache.setObject(object as NSCoding, forKey: key)
    }
    public class func values(key: String) -> (json :JSON,empty :Bool){
        guard let cache = bookCache else { return (JSON(),true)}
        let object = cache.object(forKey: key)
        let json = JSON(object as Any)
        return (json,json.isEmpty)
    }
}
