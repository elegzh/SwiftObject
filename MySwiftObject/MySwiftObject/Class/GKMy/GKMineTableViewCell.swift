//
//  GKMyTableViewCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKMyTableViewCell: BaseTableCell {

    @IBOutlet weak var imageRight: UIImageView!
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var lineView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.lineView.backgroundColor = UIColor.appxdddddd()
        self.titleLab.textColor = UIColor.appx333333()
    }
    var model : GKMyModel?{
        didSet{
            guard let item = model else { return }
            self.titleLab.text = item.title
            self.imageV.image = UIImage(named: item.icon)
            self.switchBtn.isHidden = true
          
            if let switchs = model?.switchOn{
                self.switchBtn.isHidden = false
                self.switchBtn.isOn = switchs
            }
            self.imageRight.isHidden = !self.switchBtn.isHidden
            
        }
    }
    @IBAction func changeAction(_ sender: UISwitch) {
        let night = sender.isOn
        ApiClient.setNight(night)
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate,let window = appDelegate.window{
            window.overrideUserInterfaceStyle = night ? .dark : .light
            NotificationCenter.default.post(name: AppThemeNotification, object:UIColor(hex:"000000"))
        }
    }
    
}
