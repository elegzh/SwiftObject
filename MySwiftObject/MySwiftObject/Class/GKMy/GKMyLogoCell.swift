//
//  GKMyLogoCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/3/31.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKMyLogoCell: UICollectionViewCell {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var check: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageV.layer.masksToBounds = true
        self.imageV.layer.cornerRadius = 5
    }
    @IBOutlet weak var more: UIMenu!
}
