//
//  GKMySetCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/5/18.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKMySetCell: BaseTableCell {
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var imageRight: UIImageView!
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var lineView: UIView!
    
    weak var delegate :GKMySwitchDelegate? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLab.textColor = UIColor.appx000000()
        self.lineView.backgroundColor = UIColor.appxdddddd()
    }
    var model :GKMyModel?{
        didSet{
            guard let item = model else { return }
            self.titleLab.text = item.title
            self.switchBtn.isHidden = true
            if let switchs = model?.switchOn{
                self.switchBtn.isHidden = false
                self.switchBtn.isOn = switchs
            }
            self.imageRight.isHidden = !self.switchBtn.isHidden
        }
    }
    @IBAction func changeAction(_ sender: UISwitch) {
        guard let delegate = self.delegate else { return }
        guard let item = self.model else { return }
        delegate.switchAction(sender: sender, model:item)
    }
}
