//
//  GKShopController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKShopController: BaseTableViewController {
    private lazy var listData : [String] = {
        return []
    }()
    private lazy var controllers : [GKShopTabController] = {
        return []
    }()
    lazy var moreBtn: UIButton = {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        button.setImage(UIImage(named: "ic_strategy_search"), for: .normal)
        button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        button.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
        return button
    }()
    private lazy var magicViewCtrl: VTMagicController = {
        let ctrl = VTMagicController()
        
        ctrl.magicView.navigationInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        ctrl.magicView.switchStyle = .default
        ctrl.magicView.layoutStyle = .default
        ctrl.magicView.itemSpacing = 20
        ctrl.magicView.navigationHeight = 44
        ctrl.magicView.separatorHeight = 0.5;
        ctrl.magicView.backgroundColor = UIColor.clear
        ctrl.magicView.separatorColor = UIColor.clear
        ctrl.magicView.navigationColor = UIColor.appxffffff()
        
        ctrl.magicView.sliderExtension = 1;
        ctrl.magicView.bubbleRadius = 1;
        ctrl.magicView.sliderWidth = 25;
        ctrl.magicView.sliderHeight = 2;
        ctrl.magicView.sliderOffset = -3
        
        ctrl.magicView.isAgainstStatusBar = false;
        ctrl.magicView.dataSource = self;
        ctrl.magicView.delegate = self;
        ctrl.magicView.needPreloading = true;
        ctrl.magicView.bounces = false;
        ctrl.magicView.rightNavigatoinItem = self.moreBtn
        return ctrl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadUI()
        
    }
    private func loadUI(){
        self.fd_prefersNavigationBarHidden = true
        self.magicViewCtrl.magicView.sliderColor = UIColor.appx000000()
        self.addChild(self.magicViewCtrl)
        self.view.addSubview(self.magicViewCtrl.view)
        self.magicViewCtrl.view.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalToSuperview().offset(STATUS_BAR_HIGHT)
        }
        self.setupRefresh(scrollView: self.tableView, options: .auto)
        self.magicViewCtrl.view.isHidden = true
    }
    override func refreshData(page: Int) {
        ApiMoya.request(target: .homeSex) { json in
            guard let info = GKRankInfo.deserialize(from: json.rawString())else{
                return
            }
            if let list = info.male,list.count > 0{
                let vc = GKShopTabController(listData: list)
                self.listData.append("男生")
                self.controllers.append(vc)
            }
            if let list = info.female,list.count > 0{
                let vc = GKShopTabController(listData: list)
                self.listData.append("女生")
                self.controllers.append(vc)
            }
            if let list = info.picture,list.count > 0{
                let vc = GKShopTabController(listData: list)
                self.listData.append("精选")
                self.controllers.append(vc)
            }
            if let list = info.epub,list.count > 0{
                let vc = GKShopTabController(listData: list)
                self.listData.append("其他")
                self.controllers.append(vc)
            }
            self.magicViewCtrl.magicView.reloadData()
            self.endRefresh(more: false)
            self.magicViewCtrl.view.isHidden = false
            self.tableView.isHidden = true
        } failure: { error in
            self.endRefreshFailure(error: error)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }
    @objc func searchAction(){
        GKJump.jumpToSearch()
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
}
extension GKShopController :VTMagicViewDataSource,VTMagicViewDelegate{
    func menuTitles(for magicView: VTMagicView) -> [String] {
        return self.listData
    }
    func magicView(_ magicView: VTMagicView, menuItemAt itemIndex: UInt) -> UIButton {
        let button  = magicView.dequeueReusableItem(withIdentifier: "com.home.btn.itemIdentifier") ?? UIButton(type: .custom)
        button.setTitle(self.listData[Int(itemIndex)], for: .normal)
        button.setTitleColor(UIColor.appx333333(), for: .normal)
        button.setTitleColor(UIColor.appx000000(), for: .selected)
        button.titleLabel?.font = UIFont.systemFont(ofSize:18, weight: .medium)
        return button
    }
    func magicView(_ magicView: VTMagicView, viewControllerAtPage pageIndex: UInt) -> UIViewController {
        return self.controllers[Int(pageIndex)]
    }
}
