//
//  GKRankInfo.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/17.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKRankModel: HandyJSON {
    var rankId    :String?  = nil
    var monthRank :String?  = nil
    var totalRank :String?  = nil
    
    var collapse  :String? = ""
    var cover     :String? = ""
    var shortTitle:String? = ""
    var title     :String? = ""
    var select    :Bool?   = false

    func mapping(mapper: HelpingMapper) {
        mapper <<< self.rankId <-- ["rankId","_id"]
    }
    required init() {}
    
}
class GKRankInfo: HandyJSON {
    var male   :[GKRankModel]? = nil
    var female :[GKRankModel]? = nil
    var picture:[GKRankModel]? = nil
    var epub   :[GKRankModel]? = nil
    var state:Int? = 0;
    
    var boyDatas :[GKRankModel]{
        get{
//            guard let listData = moya.client.rankDatas else { return self.male ?? [] }
//            if listData.count > 0 {
//                listData.forEach { (objc) in
//                    let list = self.male.filter({ (item) -> Bool in
//                        item.rankId == objc.rankId
//                    })
//                    if list.count > 0 {
//                        if let item = list.first{
//                            item.select = true
//                        }
//                    }
//                }
//            }
            return self.male ?? []
        }
    }
    var girlDatas :[GKRankModel]{
        get{
//            guard let listData = moya.client.rankDatas else { return self.female }
//            if listData.count > 0 {
//                listData.forEach { (objc) in
//                    let list = self.female.filter({ (item) -> Bool in
//                        item.rankId == objc.rankId
//                    })
//                    if let item = list.first{
//                        item.select = true
//                    }
//                }
//            }
            return self.female ?? []
        }
    }
    required init() {}
}
