//
//  GKTabBarController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import MGJRouter_Swift

class GKTabBarController: UITabBarController {
    private lazy var listData: [UIViewController] = {
        return [];
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        debugPrint("当前设备是否是iPhoneX:\(iPhone_X)")
        self.tabBar.isTranslucent = false
        if let vc = MGJRouter.object(AppShelf) as? UIViewController{
            self.createCtrl(vc: vc, title:"书架", normal:"icon_tabbar_video_n", select:"icon_tabbar_video_h")
        }

        if let vc = MGJRouter.object(AppShop) as? UIViewController{
            self.createCtrl(vc: vc, title:"书城", normal:"icon_tabbar_home_n", select:"icon_tabbar_home_h")
        }
        if let vc = MGJRouter.object(AppClassify) as? UIViewController{
            self.createCtrl(vc: vc, title:"分类", normal:"icon_tabbar_found_n", select:"icon_tabbar_found_h")
        }
        if let vc = MGJRouter.object(AppMy) as? UIViewController{
            self.createCtrl(vc: vc, title:"我的", normal:"icon_tabbar_wall_n", select:"icon_tabbar_wall_h")
        }
        self.viewControllers = self.listData
        self.tabBar.setTabbarColor()
    }
    private func loadUI(){
        
    }
    private func createCtrl(vc :UIViewController,title :String,normal: String,select :String) {
        let nv = BaseNavigationController(rootViewController: vc)
        vc.showNavTitle(title: title)
        nv.tabBarItem.title = title
        if let image = UIImage(named: normal)?.withRenderingMode(.alwaysOriginal),let selectedImage = UIImage(named: select)?.withRenderingMode(.alwaysOriginal) {
            nv.setTabItem(normal: image, selectedImage: selectedImage)
        }
        self.listData.append(nv)
    }
    override var shouldAutorotate: Bool{
        guard let visibleViewController = self.selectedViewController else { return false }
        return visibleViewController.shouldAutorotate;
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        guard let visibleViewController = self.selectedViewController else { return .portrait }
        return visibleViewController.supportedInterfaceOrientations;
    }
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        guard let visibleViewController = self.selectedViewController else { return .portrait }
        return visibleViewController.preferredInterfaceOrientationForPresentation;
    }
}
