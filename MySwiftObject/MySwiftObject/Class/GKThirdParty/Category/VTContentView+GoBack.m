//
//  VTContentView+GoBack.m
//  MySwiftObject
//
//  Created by wangws1990 on 2020/9/9.
//  Copyright © 2020 wangws1990. All rights reserved.
//

#import "VTContentView+GoBack.h"
  
@implementation VTContentView (GoBack)
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (self.contentOffset.x <= 0) {
        if ([otherGestureRecognizer.delegate isKindOfClass:NSClassFromString(@"_FDFullscreenPopGestureRecognizerDelegate")]) {
            return YES;
        }
    }
    return NO;
}
@end
